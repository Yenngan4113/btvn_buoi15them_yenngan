document.querySelector("#tinh_thue").addEventListener("click", function () {
  let userName = document.querySelector("#name").value;
  let tongThuNhap = +document.querySelector("#salary").value;
  let nguoiPhuThuoc = +document.querySelector("#nguoi_phu_thuoc").value;
  let thuNhapChiuThue = tongThuNhap - nguoiPhuThuoc * 1.6e6 - 4e6;
  let tienThue = 0;
  if (thuNhapChiuThue <= 60e6) {
    tienThue = thuNhapChiuThue * 0.05;
  } else if (thuNhapChiuThue <= 120e6) {
    tienThue = 60e6 * 0.05 + (thuNhapChiuThue - 60e6) * 0.1;
  } else if (thuNhapChiuThue <= 210e6) {
    tienThue = 60e6 * 0.05 + 60e6 * 0.1 + (thuNhapChiuThue - 120e6) * 0.15;
  } else if (thuNhapChiuThue <= 384e6) {
    tienThue =
      60e6 * 0.05 +
      60e6 * 0.1 +
      (210e6 - 120e6) * 0.15 +
      (thuNhapChiuThue - 210e6) * 0.2;
  } else if (thuNhapChiuThue <= 624e6) {
    tienThue =
      60e6 * 0.05 +
      60e6 * 0.1 +
      (210e6 - 120e6) * 0.15 +
      (384e6 - 210e6) * 0.2 +
      (thuNhapChiuThue - 384e6) * 0.25;
  } else if (thuNhapChiuThue <= 960e6) {
    tienThue =
      60e6 * 0.05 +
      60e6 * 0.1 +
      (210e6 - 120e6) * 0.15 +
      (384e6 - 210e6) * 0.2 +
      (624e6 - 384e6) * 0.25 +
      (thuNhapChiuThue - 624e6) * 0.3;
  } else {
    tienThue =
      60e6 * 0.05 +
      60e6 * 0.1 +
      (210e6 - 120e6) * 0.15 +
      (384e6 - 210e6) * 0.2 +
      (624e6 - 384e6) * 0.25 +
      (960e6 - 624e6) * 0.3 +
      (thuNhapChiuThue - 960e6) * 0.35;
  }
  let formatTienThue = Intl.NumberFormat("vi-VI", {
    style: "currency",
    currency: "VND",
  }).format(tienThue);
  document.querySelector(
    "#KQ_bai1"
  ).innerHTML = `${userName} - Số người phụ thuộc: ${nguoiPhuThuoc} - ${formatTienThue}`;
});
// Bài 2
let chooseOption = document.querySelector("#selection");
chooseOption.addEventListener("change", function () {
  if (chooseOption.value == "business") {
    document.querySelector("#connection_quantity").style.display = "block";
  } else {
    document.querySelector("#connection_quantity").style.display = "none";
  }
});
document.querySelector("#tinh_tien").addEventListener("click", function () {
  let phiDVCoBan = 0;
  let phiXuLy = 0;
  let tienCap = 0;
  let maKH = document.querySelector("#maKH").value;
  let soLuongKetNoi = +document.querySelector("#sl_ket_noi").value;
  let soKenh = +document.querySelector("#so_kenh").value;
  let loaiKH = document.querySelector("#selection").value;
  if (loaiKH == "business") {
    phiXuLy = 15;

    if (soLuongKetNoi > 10) {
      phiDVCoBan = 75 + (soLuongKetNoi - 10) * 5;
      tienCap = phiXuLy + phiDVCoBan + soKenh * 50;
    } else {
      phiDVCoBan = 75;
      tienCap = phiXuLy + 75 + soKenh * 50;
    }
  } else {
    phiXuLy = 4.5;
    phiDVCoBan = 20.5;
    tienCap = phiXuLy + phiDVCoBan + soKenh * 7.5;
  }
  let formatTienCap = Intl.NumberFormat("us-US", {
    style: "currency",
    currency: "USD",
  }).format(tienCap);
  document.querySelector(
    "#KQ_bai2"
  ).innerHTML = `Mã khách hàng là ${maKH} - Tiền cáp ${formatTienCap}`;
});
